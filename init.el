;;;; tpb-init.el -*- lexical-binding: t; -*-
;;; Setup
;;;; Variables
(defconst is-windows-p (string-equal system-type 'windows-nt))
(defconst is-laptop-p (string-equal (system-name) 'lepidoptera))
(defconst is-pc-p (string-equal (system-name) 'heterocera))
;; REVIEW: I like that eglot is more native, but for now it is
;; missing a few features:
;; - [ ] Rust inlay hints
;; - [x] UPDATE: consult-eglot exists. Symbol search (consult-lsp for lsp-mode)
;; - [ ]Download server binaries
;; HOWEVER...eglot works with tramp while lsp-mode
;; hangs on startup https://github.com/emacs-lsp/lsp-mode/issues/2709
(defconst lsp-engine 'eglot)
(defconst lsp-engine-lsp-mode-p (string-equal lsp-engine "lsp-mode"))
(defconst lsp-engine-eglot-p (string-equal lsp-engine "eglot"))

;;;; Paths
;;; Startup
(push '(menu-bar-lines . 0)   default-frame-alist)
(push '(tool-bar-lines . 0)   default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)

(setq inhibit-startup-message t
      inhibit-startup-echo-area-message user-login-name
      inhibit-default-init t
      initial-major-mode 'fundamental-mode
      initial-scratch-message nil
      ring-bell-function #'ignore
      visible-bell nil)

(setq tab-always-indent 'complete)
;;; Optimization
(setq-default
 bidi-display-reordering 'left-to-right
 bidi-paragraph-direction 'left-to-right
 cursor-in-non-selected-windows . nil)

(setq
 auto-mode-case-fold nil
 highlight-nonselected-windows nil
 fast-but-imprecise-scrolling t
 h-scroll-margin 2
 h-scroll-step 1
 scroll-conservatively 101
 scroll-margin 0
 scroll-presever-screen-position t
 ffap-machine-p-known 'reject
 frame-inhibit-implied-resize t
 idle-update-delay 1.0
 inhibit-compacting-font-caches t
 redisplay-skip-fontification-on-input t)

;;; Package Management
;;;; Straight
;; must be set before bootstrap
(setq straight-repository-branch "develop"
      straight-check-for-modifications nil
      straight-vc-git-default-clone-depth 1)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	    (url-retrieve-synchronously
	     "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	     'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;;;; Use-package
(straight-use-package 'use-package)

(setq straight-use-package-by-default t
      use-package-hook-name-suffix nil
      use-package-alawys-defer t)

;;;; Load updated org early so packages aren't built against builtq-in org
(straight-register-package 'org)
(straight-use-package 'org)

(use-package straight
  :if is-windows-p
  :custom
  ;; Straight's find process fails when using Windows' system executable
  (straight-find-executable "C:\\msys64\\usr\\bin\\find.exe"))

(use-package blackout)

(use-package esup
  :disabled t
  :custom
  (esup-depth 0))

(use-package recentf
  :straight (:type built-in)
  :hook (after-init-hook . recentf-mode)
  :bind
  ("C-c f r" . recentf-open-files)
  :config
  (add-to-list 'recentf-exclude "bookmarks"))

(setq
 menu-bar-mode nil
 tool-bar-mode nil
 scroll-bar-mode nil)

(use-package emacs
  :bind
  ("M-o" . other-window)
  ;; toggles
  ("C-c t i" . highlight-indent-guides-mode)
  ("C-c t n"  . display-line-numbers-mode)
  ("C-c t t"  . load-theme)
  ("C-c t h"  . hl-line-mode)
  ;; file operations
  ("C-c f f" . find-file)
  ("C-c f s" . save-buffer)
  ;; buffer operations
  ("C-c b b" . switch-to-buffer)
  ("C-c b q" . kill-buffer)
  ("C-c b Q" . kill-this-buffer)
  ("C-c b n" . next-buffer)
  ("C-c b p" . previous-buffer)
  ("C-c a"   . org-agenda)
  ;; window operations
  ("C-c w s" . split-window-below)
  ("C-c w v" . split-window-right)
  ("C-c w q" . delete-window)
  ("C-c w o" . delete-other-windows)
  ("C-c q q" . save-buffers-kill-terminal))

;;; GCMH
(use-package gcmh
  :blackout gcmh-mode
  :hook (after-init-hook . gcmh-mode)
  :custom
  (gcmh-idle-delay 0.5)
  (gcmh-high-cons-threshold (* 16 1024 1024))) ;;16mb (* 16 1024 1024)

;;; Completion
;;;; Vertico
(use-package vertico
  :straight
  (vertico :includes vertico-directory
           :files (:defaults "extensions/vertico-directory.el"))
  :hook
  (after-init-hook . vertico-mode)
  (rfn-eshadow-update-overlay-hook . vertico-directory-tidy)
  :custom
  (vertico-resize nil)
  (vertico-count 17)
  (vertico-cycle t)
  (completion-in-region-function
   (lambda (&rest args)
	 (apply (if vertico-mode
			    #'consult-completion-in-region
			  #'completion--in-region)
		    args)))
  :bind (:map vertico-map
			  ("RET" . vertico-directory-enter)
			  ("DEL" . vertico-directory-delete-char)
			  ("M-DEL" . vertico-directory-delete-word)))

;;;; Embark
(use-package embark
  :custom
  (which-key-use-C-h-commands nil)
  (prefix-help-command #'embark-prefix-help-command)
  :bind
  ([remap describe-bindings] . embark-bindings)
  ("C-;"               . embark-act)
  (:map minibuffer-local-map
		("C-;"               . embark-act)
		("C-c C-;"           . embark-export)))

(use-package embark-consult
  :after (embark consult)
  :hook (embark-collect-mode-hook . consult-preview-at-point-mode))

(use-package  wgrep
  :custom
  (wgrep-auto-save-buffer t))

;;;; Marginalia
(use-package marginalia
  :hook
  (after-init-hook . marginalia-mode)
  (marginalia-mode-hook . all-the-icons-completion-marginalia-setup)
  :bind
  (:map minibuffer-local-map
		("M-A" . marginalia-cycle)))

;;;; Consult
(use-package consult
  :bind (
		 ("C-c s b"                        . consult-line)
         ("C-c s g"                        . consult-ripgrep)
		 ([remap recentf-open-files]                        . consult-recent-file)
		 ([remap list-buffers]                        . consult-buffer)
		 ([remap load-theme]                        . consult-theme)
		 ([remap apropos]                       . consult-apropos)
		 ([remap bookmark-jump]                 . consult-bookmark)
		 ([remap goto-line]                     . consult-goto-line)
		 ([remap imenu]                         . consult-imenu)
		 ([remap locate]                        . consult-locate)
		 ([remap load-theme]                    . consult-theme)
		 ([remap man]                           . consult-man)
		 ([remap switch-to-buffer]              . consult-buffer)
		 ([remap switch-to-buffer-other-window] . consult-buffer-other-window)
		 ([remap switch-to-buffer-other-frame]  . consult-buffer-other-frame) ;
		 ([remap yank-pop]                      . consult-yank-pop))
  :init
  (advice-add #'completing-read-multiple :override #'consult-completing-read-multiple)
  (advice-add #'multi-occur :override #'consult-multi-occur)
  :custom
  (consult-narrow-key "<")
  (consult-line-numbers-widen t)
  (consult-async-min-input 2)
  (consult-async-refresh-delay 0.15)
  (consult-async-input-throttle 0.2)
  (consult-async-input-debounce 0.1)
  (consult-project-root-function
   (lambda ()
     (when-let (project (project-current))
       (car (project-roots project)))))
  :config
  (consult-customize
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-recent-file consult--source-project-recent-file consult--source-bookmark
   :preview-key (kbd "M-.")))

;;;; Corfu
(use-package corfu
  :init
  (corfu-global-mode)
  :custom
  (corfu-quit-at-boundary nil))

;; Add extensions
(use-package corfu-doc
  :straight (:host github :repo "galeo/corfu-doc")
  :hook
  (corfu-mode-hook . corfu-doc-mode)
  :bind
  (:map corfu-map
        ("M-p" . corfu-doc-scroll-down)
        ("M-n" . corfu-doc-scroll-up)))

(use-package cape
  ;; Bind dedicated completion commands
  :bind
  ("C-c p p" . completion-at-point) ;; capf
  ("C-c p t" . complete-tag)        ;; etags
  ("C-c p d" . cape-dabbrev)        ;; or dabbrev-completion
  ("C-c p f" . cape-file)
  ("C-c p k" . cape-keyword)
  ("C-c p s" . cape-symbol)
  ("C-c p a" . cape-abbrev)
  ("C-c p i" . cape-ispell)
  ("C-c p l" . cape-line)
  ("C-c p w" . cape-dict)
  :init
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  (dolist (backend '( cape-keyword cape-file cape-dabbrev))
	(add-to-list 'completion-at-point-functions backend))
  ;;(add-to-list 'completion-at-point-functions #'cape-abbrev)
  ;;(add-to-list 'completion-at-point-functions #'cape-ispell)
  ;;(add-to-list 'completion-at-point-functions #'cape-dict)
  ;;(add-to-list 'completion-at-point-functions #'cape-symbol)
  ;;(add-to-list 'completion-at-point-functions #'cape-line)
  )

(use-package kind-icon
  :after corfu
  :custom
  (kind-icon-default-face 'corfu-default) ; to compute blended backgrounds correctly
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

;;;; Orderless
(use-package orderless
  :after vertico
  :config
  (defun +vertico-orderless-dispatch (pattern _index _total)
	(cond
	 ;; Ensure $ works with Consult commands, which add disambiguation suffixes
	 ((string-suffix-p "$" pattern)
	  `(orderless-regexp . ,(concat (substring pattern 0 -1) "[\x100000-\x10FFFD]*$")))
	 ;; Ignore single !
	 ((string= "!" pattern) `(orderless-literal . ""))
	 ;; Without literal
	 ((string-prefix-p "!" pattern) `(orderless-without-literal . ,(substring pattern 1)))
	 ((string-suffix-p "!" pattern) `(orderless-without-literal . ,(substring pattern 1)))
	 ;; Character folding
	 ((string-prefix-p "%" pattern) `(char-fold-to-regexp . ,(substring pattern 1)))
	 ((string-suffix-p "%" pattern) `(char-fold-to-regexp . ,(substring pattern 0 -1)))
	 ;; Initialism matching
	 ((string-prefix-p "`" pattern) `(orderless-initialism . ,(substring pattern 1)))
	 ((string-suffix-p "`" pattern) `(orderless-initialism . ,(substring pattern 0 -1)))
	 ;; Literal matching
	 ((string-prefix-p "=" pattern) `(orderless-literal . ,(substring pattern 1)))
	 ((string-suffix-p "=" pattern) `(orderless-literal . ,(substring pattern 0 -1)))
	 ;; Flex matching
	 ((string-prefix-p "~" pattern) `(orderless-flex . ,(substring pattern 1)))
	 ((string-suffix-p "~" pattern) `(orderless-flex . ,(substring pattern 0 -1)))))
  :custom
  (orderless-style-dispatchers '(+vertico-orderless-dispatch))
  (orderless-component-separator  "[ &]")
  (completion-styles (quote(orderless)))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles partial-completion)))))

;;; UI
(use-package writeroom-mode
  :bind
  ("C-c z" . writeroom-mode))

(use-package hl-line
  :hook
  (prog-mode-hook . hl-line-mode))

(use-package dashboard
  :disabled t ;; looks nice, but not useful
  :custom
  (dashboard-startup-banner 'logo)
  (dashboard-center-content t)
  (dashboard-set-footer nil)
  :config
  (dashboard-setup-startup-hook))

(use-package window-divider
  :straight (:type built-in)
  :hook (after-init-hook . window-divider-mode)
  :custom
  (window-divider-default-places t)
  (window-divider-default-bottom-width 1)
  (window-divider-default-right-width 1))

(use-package minibuffer
  :straight (:type built-in)
  :custom
  (enable-recursive-minibuffers t)
  (echo-keystrokes 0.02)
  (resize-mini-windows 'grow-only))

(use-package fringe
  :straight nil
  :custom
  (indicate-buffer-boundaries nil)
  (indicate-empty-lines nil))

(use-package frame
  :straight nil
  :custom
  (frame-resize-pixelwise t)
  (window-resize-pixelwise nil))

(use-package windmove
  :straight (:type built-in)
  :config
  (windmove-default-bindings)
  :bind
  ("C-c w h" . windmove-left)
  ("C-c w l" . windmove-right))

(use-package help
  :straight nil
  :hook
  (help-mode-hook . buffer-face-mode))

;;;; Indent Guide
(use-package highlight-indent-guides
  :blackout highlight-indent-guides-mode
  :hook
  ((prog-mode-hook conf-mode-hook) . highlight-indent-guides-mode)
  :custom
  (highlight-indent-guides-method 'character))

;;;; hl-todo
(use-package hl-todo
  :hook (prog-mode-hook . hl-todo-mode)
  :custom
  (hl-todo-highlight-punctuation ":")
  (hl-todo-keyword-faces
   `(;; For things that need to be done, just not today.
     ("TODO" warning bold)
     ;; For problems that will become bigger problems later if not
     ;; fixed ASAP.
     ("FIXME" error bold)
     ;; For tidbits that are unconventional and not intended uses of the
     ;; constituent parts, and may break in a future update.
     ("HACK" font-lock-constant-face bold)
     ;; For things that were done hastily and/or hasn't been thoroughly
     ;; tested. It may not even be necessary!
     ("REVIEW" font-lock-keyword-face bold)
     ;; For especially important gotchas with a given implementation,
     ;; directed at another user other than the author.
     ("NOTE" success bold)
     ;; For things that just gotta go and will soon be gone.
     ("DEPRECATED" font-lock-doc-face bold)
     ;; For a known bug that needs a workaround
     ("BUG" error bold)
     ;; For warning about a problematic or misguiding code
     ("XXX" font-lock-constant-face bold))))

;;;; Treemacs
(use-package treemacs
  :hook
  (treemacs-mode-hook . buffer-face-mode)
  :bind
  ("C-c o p" . treemacs)
  :config
  (treemacs-load-theme "all-the-icons"))

(use-package treemacs-python
  :straight nil
  :no-require t
  :if (string-equal system-type 'windows-nt)
  :custom
  (treemacs-python-executable  "c:/msys64/mingw64/bin/python.exe"))

(use-package treemacs-all-the-icons
  :init
  (with-eval-after-load "treemacs"
	(require 'treemacs-all-the-icons)))

;;;; Tabs
(use-package all-the-icons
  :custom
  (all-the-icons-scale-factor 1.1))

(use-package tab-line
  :straight (:type built-in)
  :config
  (global-tab-line-mode))

;;;; Modeline

(use-package mood-line
  :disabled t
  :config
  (mood-line-mode 1))

(use-package moody
  :disabled t)

(use-package doom-modeline
  :custom
  (doom-modeline-hud t)
  :config
  (doom-modeline-mode))

;;;; Solaire
(use-package solaire-mode
  :hook
  (after-init-hook . solaire-global-mode))

;;;; Themes
(use-package zenburn-theme
  :no-require t
  :commands
  zenburn-with-color-variables
  :custom
  ;; custom wasn't working for some reason
  (zenburn-scale-org-headlines t)
  :config
  (setq custom--inhibit-theme-enable nil)
  (zenburn-with-color-variables
   (custom-theme-set-faces    'zenburn
    `(tab-line ((t (:background ,zenburn-bg-1))))
    `(tab-line-tab ((t (:background ,zenburn-bg-05 :fg ,zenburn-fg-1))))
    `(tab-line-tab-current ((t (:background ,zenburn-bg :fg ,zenburn-fg+1))))
    `(tab-line-tab-inactive ((t (:background ,zenburn-bg-1))))
    `(tab-line-tab-modified ((t (:foreground ,zenburn-orange))))
    `(tab-line-separator ((t (:background ,zenburn-bg+2)))))))

(use-package vscode-dark-plus-theme
  :no-require t)

(use-package nord-theme
  :no-require t)

(use-package dracula-theme
  :disabled t
  :no-require t
  :custom-face
  ;; more like vscode theme
  (mode-line ((t (:background "#21222c" :box nil))))
  (mode-line-inactive ((t (:background "#21222c" :foreground "#6272a4" :box nil))))
  (solaire-default-face ((t (:background "#21222c"))))
  (tab-line ((t (:background "#191a21"))))
  (tab-bar ((t (:background "#191a21"))))
  (line-number ((t (:foreground "#6272a4"))))
  (line-number-current-line ((t (:fopreground "#c6c6c6"))))
  (window-divider ((t (:foreground "#373844"))))
  (solaire-hl-line-face ((t (:background "#44457a"))))
  (hl-line ((t (:background nil :box (:line-width -1 :color "#44475a"))))))

(use-package modus-themes
  :init
  (with-eval-after-load 'org-src
    (add-to-list 'org-src-block-faces '("rust"  modus-themes-nuanced-blue))
    (add-to-list 'org-src-block-faces '("config-space" modus-themes-nuanced-green))
    (add-to-list 'org-src-block-faces '("sql"  modus-themes-nuanced-cyan)))
  :custom
  (modus-themes-org-agenda
   '((header-block . (variable-pitch scale-title))
     (header-date . (grayscale bold-all))))
  (modus-themes-italic-constructs t)
  (modus-themes-bold-constructs nil)
  (modus-themes-completions (quote ((matches . (background intense))
                  (selection . (accented intense semibold))
                  (popup . (accented)))))
  (modus-themes-fringes 'subtle)
  (modus-themes-variable-pitch-ui t)
  (modus-themes-syntax '(alt-syntax))
  (modus-themes-org-blocks 'tinted-background)
  (modus-themes-tabs-accented nil)
  (modus-themes-mixed-fonts t)
  (modus-themes-mode-line '(borderless))
  (modus-themes-headings
   '((1 . (1.3 ))
     (2 . (1.2 ))
     (3 . (1.10 ))))
  (modus-themes-operandi-color-overrides
   '())
  (modus-themes-vivendi-color-overrides
   '()))

(use-package modus-themes
  :if is-windows-p
  :config
  (modus-themes-load-operandi))

(use-package cobalt2-theme
  :no-require t
  :straight (:type git :host gitlab :repo "__tpb/cobalt2-emacs-theme")
  :config
  (set-face-attribute 'tab-line nil :inherit 'variable-pitch)
  (set-face-attribute 'mode-line nil :inherit 'variable-pitch))

(use-package dbus
  :unless is-windows-p
  :straight (:type built-in)
  :demand t
  :init
  (defun theme-switcher (value)
     (pcase value
          ;; No Preference
       (0 (shell-command "gsettings set org.gnome.desktop.interface gtk-theme \"adw-gtk3\"")
          (modus-themes-load-operandi))
           ;; Prefers dark
       (1 (shell-command "gsettings set org.gnome.desktop.interface gtk-theme \"adw-gtk3-dark\"")
          (modus-themes-load-vivendi))
           ;; Prefers light. Not currently used by Gnome
       (2 (shell-command "gsettings set org.gnome.desktop.interface gtk-theme \"adw-gtk3\"")
          (modus-themes-load-operandi))
           (_ (message "Invalid key value"))))

  (defun handler (value)
    (theme-switcher (car (car value))))

  (defun signal-handler (namespace key value)
    (if (and
         (string-equal namespace "org.freedesktop.appearance")
         (string-equal key "color-scheme"))
        (theme-switcher (car value))))
  :config
  (dbus-call-method-asynchronously
   :session
   "org.freedesktop.portal.Desktop"
   "/org/freedesktop/portal/desktop"
   "org.freedesktop.portal.Settings"
   "Read"
   #'handler
   "org.freedesktop.appearance"
   "color-scheme")

  (dbus-register-signal
   :session
   "org.freedesktop.portal.Desktop"
   "/org/freedesktop/portal/desktop"
   "org.freedesktop.portal.Settings"
   "SettingChanged"
   #'signal-handler))

(when is-laptop-p (modus-themes-load-vivendi))

;;;; Font
(use-package pc-font
  :no-require t
  :straight nil
  :if is-pc-p
  :init
  (set-face-attribute 'default nil
				      :font "hack"
				      :height 100)
  (set-face-attribute 'fixed-pitch nil
				      :font "hack"
				      :height 100)
  (set-face-attribute 'variable-pitch nil
				      :font "liberation sans"
				      :height 110))

(use-package laptop-font
  :no-require t
  :straight nil
  :if is-laptop-p
  :init
  (set-face-attribute 'default nil
				      :font "IBM Plex Mono Text"
				      :height 105)
  (set-face-attribute 'fixed-pitch nil
				      :font "IBM Plex Mono Text"
				      :height 105)
  (set-face-attribute 'variable-pitch nil
				      :font "IBM Plex Sans Text"
                      :height 110))

(use-package work-font
  :no-require t
  :straight nil
  :if is-windows-p
  :init
  (set-face-attribute 'default nil
                      :font "jetbrains mono"
                      :height 90)
  (set-face-attribute 'fixed-pitch nil
                      :font "jetbrains mono"
                      :height 90)
  (set-face-attribute 'variable-pitch nil
                      ;; :font "Cantarell"
                      :font "calibri"
                      :height 110))


;;;; Icons
(use-package all-the-icons-completion
  :hook (after-init-hook . all-the-icons-completion-mode))

;;;; Mixed Pitch
(use-package mixed-pitch
  :blackout t
  ;;:hook
  ;; If you want it in all text modes:n
  ;; (text-mode-hook . mixed-pitch-mode)
  :custom
  (mixed-pitch-set-height 120)
  :config
  (add-to-list 'mixed-pitch-fixed-pitch-faces
               '(org-date
                 ;;org-footnote
                 org-special-keyword
    		     org-property-value
			     org-ref-cite-face
			     ;; org-tag
			     org-todo
			     org-done
			     font-lock-comment-face)))

(use-package face-remap
  :hook
  (text-mode-hook . variable-pitch-mode))

;;; Editor
(use-package windows-nt
  :no-require t
  :straight nil
  :if (string-equal system-type 'windows-nt)
  :custom
  (selection-coding-system 'utf-16-le)
  (w32-get-true-file-attributes nil)   ; decrease file IO workload
  (w32-pipe-read-delay 0)              ; faster IPC
  (w32-pipe-buffer-size 65536))

(use-package editor
  :straight (:type built-in)
  :custom
  (find-file-visit-truename t)
  (vc-follow-symlinks t)
  (create-lockfiles nil)
  (make-backup-files nil)
  (auto-save-default t)
  (indent-tabs-mode nil)
  (tab-width 4)
  (tabify-regexp  "^\t* [ \t]+")
  (default-fill-column 80)
  (word-wrap t)
  (truncate-lines t)
  (require-final-new-line t)
  (sentence-end-double-space nil)
  (truncate-partial-width-windows nil)
  (kill-do-not-save-duplicates t)
  (blink-cursor-mode nil)
  (blink-matching-paren nil)
  (x-stretch-cursor nil)
  (x-underline-at-descent-line t)
  (cursor-type 'box)
  :hook (text-mode-hook . visual-line-mode))

(use-package eldoc
  :straight nil
  :blackout eldoc-mode)

(use-package paren
  :straight (:type built-in)
  ;; highlight matching delimiters
  :hook (after-init-hook . show-paren-mode)
  :custom
  (show-paren-delay 0.1)
  (show-paren-highlight-openparen t)
  (show-paren-when-point-inside-paren t)
  (show-paren-when-point-in-periphery t))

(use-package display-line-numbers
  :straight (:type built-in)
  :hook (prog-mode-hook . display-line-numbers-mode)
  :custom (display-line-numbers-width 4)
  (display-line-numbers-widen t)
  (display-line-numbers-type 'relative))

;;;; Expand Region
(use-package expand-region
  :bind
  ("C-="  . er/expand-region)
  ("C--"  . er/contract-region))

;;;; Avy Jump
(use-package avy
  :custom
  (avy-keys '(?a ?o ?e ?u ?i ?d ?h ?t ?n ?s))
  :bind
  ("C-c j j" . avy-goto-char-timer)
  ("C-c j l" . avy-goto-line))

;;;; Smart Parens
(use-package smartparens
  :blackout smartparens-mode
  :hook (prog-mode-hook . smartparens-mode)
  :custom
  (sp-navigate-skip-match nil)
  (sp-navigate-consider-sgml-tags nil)
  :config
    (sp-pair "'" nil) ;; mostly in lisp where I want only 1 ' anyhow
  ;; Autopair quotes more conservatively; if I'm next to a word/before another
  ;; quote, I don't want to open a new pair or it would unbalance them.
  (let ((unless-list '(sp-point-before-word-p
				       sp-point-after-word-p
				       sp-point-before-same-p)))
	(sp-pair "\"" nil :unless unless-list))
  ;; Expand {|} => { | }
  ;; Expand {|} => {
  ;;   |
  ;; }
  (dolist (brace '("(" "{" "["))
	(sp-pair brace nil
			 :post-handlers '(("||\n[i]" "RET") ("| " "SPC"))
			 ;; Don't autopair opening braces if before a word character or
			 ;; other opening brace. The rationale: it interferes with manual
			 ;; balancing of braces, and is odd form to have s-exps with no
			 ;; whitespace in between, e.g. ()()(). Insert whitespace if
			 ;; genuinely want to start a new form in the middle of a word.
			 :unless '(sp-point-before-word-p sp-point-before-same-p)))

  ;; In lisps ( should open a new form if before another parenthesis
  (sp-local-pair sp-lisp-modes "(" ")" :unless '(:rem sp-point-before-same-p))

  ;; Don't do square-bracket space-expansion where it doesn't make sense to
  (sp-local-pair '(emacs-lisp-mode org-mode markdown-mode gfm-mode)
			     "[" nil :post-handlers '(:rem ("| " "SPC")))

  ;; Reasonable default pairs for HTML-style comments
  (sp-local-pair (append sp--html-modes '(markdown-mode gfm-mode))
			     "<!--" "-->"
			     :unless '(sp-point-before-word-p sp-point-before-same-p)
			     :actions '(insert) :post-handlers '(("| " "SPC"))))

;;;; ws-butler
(use-package ws-butler
  :straight (ws-butler
			 :type git
			 :host github
			 :repo "hlissner/ws-butler")
  :blackout
  :commands (ws-butler-global-mode)
  :custom
  (delete-trailing-lines nil)
  (sentence-end-double-space nil)
  (word-wrap t)
  :init
  (ws-butler-global-mode)
  :config
  (setq ws-butler-global-exempt-modes
		(append ws-butler-global-exempt-modes
			    '(special-mode comint-mode term-mode eshell-mode))))

;;; Notes
;;;; Org
(use-package org
  :custom
  (org-modules '(org-id))
  (org-log-into-drawer t)
  (org-treat-S-cursor-todo-selection-as-state-change nil)
  (org-enforce-todo-dependencies t)
  (org-indirect-buffer-display 'current-window)
  (org-fontify-done-headline t)
  (org-fontify-todo-headline t)
  (org-fontify-quote-and-verse-blocks t)
  (org-fontify-whole-heading-line t)
  (org-hide-leading-stars nil)
  (org-adapt-indentation nil)
  (org-image-actual-width nil)
  (org-imenu-depth 6)
  (org-priority-faces '((?A . error)(?B . warning)(?C . success)))
  (org-startup-indented nil)
  (org-tags-column 0)
  (org-use-sub-superscripts '{})
  (org-startup-folded nil)
  (org-refile-targets
   '((nil :maxlevel . 3)
     (org-agenda-files :maxlevel . 3)))
  ;; Without this, completers like ivy/helm are only given the first level of
  ;; each outline candidates. i.e. all the candidates under the "Tasks" heading
  ;; are just "Tasks/". This is unhelpful. We want the full path to each refile
  ;; target! e.g. FILE/Tasks/heading/subheading
  (org-refile-use-outline-path 'file)
  (org-outline-path-complete-in-steps nil)
  (org-tag-alist '(("@church" . ?c)
			       ("@work" . ?w)
			       ("@home" . ?h)
			       (:newline)
			       ("inbox" . ?i)
			       ("development . ?d")
			       ("article . ?a")))
  (org-todo-keywords
   '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d@/!)")
     (sequence "WAIT(w@/!)" "HOLD(h@/!)" "|" "KILL(k@/!)" "PHONE" "MEETING")
     ))
  (org-capture-templates
   '(
     ()
     )
   )
  (org-todo-keyword-faces
   '(("NEXT"  . org-todo)
     ("WAIT" . org-scheduled)
     ("HOLD" . org-scheduled)
     ("KILL" . org-done))))

;;;; Org-Agenda
(use-package org-agenda
  :straight (:type built-in)
  :hook (org-agenda-finalize-hook . turn-off-solaire-mode)
  :custom
  (org-agenda-window-setup 'current-window)
  (org-agenda-inhibit-startup t)
  (org-agenda-files '("~/Documents/org/roam/personal_agenda.org"
                      "~/Documents/org/roam/work/work_agenda.org")))

;;;; Org-Roam-V2
(use-package org-roam
  :defer 2
  :hook
  (org-roam-mode-hook . buffer-face-mode)
  (org-roam-mode-hook . turn-on-solaire-mode)
  :custom
  (org-roam-node-display-template "${title:*} ${tags:40}")
  (org-id-link-to-org-use-id t)
  (org-roam-v2-ack t)
  (org-roam-directory "~/Documents/org/roam")
  (org-roam-dailies-directory "~/Documents/org/roam/journal")
  (org-roam-completion-everywhere t)
  (org-roam-capture-templates
   '(("d" "default" plain
      "%?"
      :if-new (file+head "${slug}.org"
                         "#+HUGO_BASE_DIR: ../\n#+HUGO_SECTION: ${Share Level}\n#+TITLE: ${title}")
      :immediate-finish t
      :unnarrowed t)

     ("m" "meeting" entry "* Meeting with %?"
      :if-new (file+olp "gtd.org"
                        "meetings"))

     ("s" "sql-table" plain
      "* Overview \n%? \n* Structure"
      :if-new (file+head "${slug}.org"
                         "#+HUGO_BASE_DIR: ../\n#+HUGO_SECTION: work\n#+TITLE: ${title}"))
     ("l" "literature" entry "* %?"
      :if-new (file+head "public/%<%Y%m%d%H%M%S>-${slug}.org"
                         "#+title: ${title}\n"))
     ))
  :bind
  ("C-c r f" . org-roam-node-find )
  ("C-c r i" . org-roam-node-insert)
  ("C-c r r" . org-roam-ref-find)
  ("C-c r c" . org-roam-capture)
  ("C-c r t" . org-roam-buffer-toggle)
  ("C-c r s" . org-roam-db-sync)
  ("C-c r w" . tpb/org-roam-context-work)
  ("C-c r h" . tpb/org-roam-context-home)
  :init
  (defun tpb/org-roam-context-work()
    "Set Org roam context to work"
    (interactive)
    (setq org-roam-directory "~/Documents/org/roam/work"
          org-roam-db-location (concat user-emacs-directory "org-roam-work.db"))
    (message "Org-roam context set to work"))
    (defun tpb/org-roam-context-home()
    "Set Org roam context to home"
    (interactive)
    (setq org-roam-directory "~/Documents/org/roam/"
          org-roam-db-location (concat user-emacs-directory "org-roam.db"))
    (message "Org-roam context set to home"))
  (with-eval-after-load 'recentf
	(add-to-list 'recentf-exclude "\\roam\\")))

(use-package org-roam-timestamps
  :after org-roam
  :config
  (org-roam-timestamps-mode)
  :custom
  (org-roam-timestamps-parent-file t)
  (org-roam-timestamps-remember-timestamps nil))

(use-package ox-hugo
  :after ox)

(use-package org-drill)

(use-package org-tree-slide)

;;; Projects
;;;; Project.el
(use-package project
  :custom
  (xref-search-program 'ripgrep))

(use-package consult-project-extra
  :after
  (consult project)
  :bind
  (("C-c p f" . consult-project-extra-find)
   ("C-c p o" . consult-project-extra-find-other-window)))

;;;; Magit
(use-package magit
  :bind
  ("C-c g" . magit-status))

;;; Programming
(use-package outline-minor-mode
  :straight (:type built-in)
  :blackout t
  :hook (prog-mode-hook . outline-minor-mode))

(use-package ob
  :straight (:type built-in)
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((sql . t)
     (shell . t))))

;;; Toolbox-tramp
(use-package toolbox-tramp
  :disabled t
  :straight (toolbox-tramp :type git
			               :host github
			               :repo "fejfighter/toolbox-tramp"))

(use-package tramp
  :straight (:type built-in)
  :config
  (push
   (cons
    "toolbox"
    '((tramp-login-program "toolbox")
      (tramp-login-args (("enter" "-c") ("%h")))
      (tramp-remote-shell "/bin/sh")
      (tramp-remote-shell-args ("-i") ("-c"))))
   tramp-methods))

;;;; Tree-sitter
(use-package tree-sitter
  :config
  (global-tree-sitter-mode)
  (use-package tree-sitter-langs)
  :hook (tree-sitter-after-on-hook . tree-sitter-hl-mode))

;;;; Lsp
(defvar +lsp--default-read-process-output-max nil)
(defvar +lsp--default-gcmh-high-cons-threshold nil)
(defvar +lsp--optimization-init-p nil)
(define-minor-mode +lsp-optimization-mode
  "Deploys universal GC and IPC optimizations for `lsp-mode' and `eglot'."
  :global t
  :init-value nil
  (if (not +lsp-optimization-mode)
      (setq-default read-process-output-max +lsp--default-read-process-output-max
                    gcmh-high-cons-threshold +lsp--default-gcmh-high-cons-threshold
                    +lsp--optimization-init-p nil)
    ;; Only apply these settings once!
    (unless +lsp--optimization-init-p
      (setq +lsp--default-read-process-output-max
            ;; DEPRECATED Remove check when 26 support is dropped
            (if (boundp 'read-process-output-max)
                (default-value 'read-process-output-max))
            +lsp--default-gcmh-high-cons-threshold
            (default-value 'gcmh-high-cons-threshold))
      ;; `read-process-output-max' is only available on recent development
      ;; builds of Emacs 27 and above.
      (setq-default read-process-output-max (* 1024 1024))
      ;; REVIEW LSP causes a lot of allocations, with or without Emacs 27+'s
      ;;        native JSON library, so we up the GC threshold to stave off
      ;;        GC-induced slowdowns/freezes. Doom uses `gcmh' to enforce its
      ;;        GC strategy, so we modify its variables rather than
      ;;        `gc-cons-threshold' directly.
      (setq-default gcmh-high-cons-threshold (* 2 +lsp--default-gcmh-high-cons-threshold))
      (gcmh-set-high-threshold)
      (setq +lsp--optimization-init-p t))))

(use-package eglot
  :if lsp-engine-eglot-p
  :hook
  (rust-mode-hook . eglot)
  (eglot--managed-mode-hook . +lsp-optimization-mode))

(use-package consult-eglot
  :if lsp-engine-eglot-p
  :after (consult eglot)
  :bind (:map eglot-mode-map
			  ([remap xref-find-apropos] . consult-eglot-symbols)))


(use-package lsp-mode
  :if lsp-engine-lsp-mode-p
  :commands lsp-mode-install-server
  :hook
  (lsp-mode-hook . +lsp-optimization-mode)
  (lsp-completion-mode . tpb/corfu-setup-lsp)
  :config
  ;; https://kristofferbalintona.me/posts/corfu-kind-icon-and-corfu-doc/
  (defun tpb/corfu-setup-lsp ()
  "Use orderless completion style with lsp-capf instead of the
  default lsp-passthrough."
  (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
        '(orderless)))
  :custom
  (lsp-log-io t)
  (lsp-completion-provider :none)
  (lsp-enable-folding nil)
  (lsp-enable-text-document-color nil)
  (lsp-enable-on-type-formatting nil))

(use-package consult-lsp
  :if lsp-engine-lsp-mode-p
  :after (consult lsp-mode)
  :init
  :bind (:map lsp-mode-map
			  ([remap xref-find-apropos] . consult-lsp-symbols)))

(use-package lsp-ui
  :if lsp-engine-lsp-mode-p
  :custom
  (lsp-ui-peek-enable t)
  (lsp-ui-sideline-enable t)
  (lsp-ui-doc-max-height 8)
  (lsp-ui-doc-max-width 35)
  (lsp-ui-doc-show-with-mouse nil)
  (lsp-ui-doc-position 'at-point)
  (lsp-ui-sideline-ignore-duplicate t)
  (lsp-ui-sideline-show-hover nil)
  (lsp-ui-sideline-show-code-actions t)
  (lsp-ui-sideline-show-diagnostics t)
  ;;(lsp-ui-sideline-actions-icon lsp-ui-sideline-actions-icon-default)
  ;; REVIEW Temporarily disabled, due to immense slowness on every
  ;;        keypress. See emacs-lsp/lsp-ui#613
  (lsp-ui-doc-enable t)
  :bind
  ("<f12>" . lsp-ui-peek-find-definitions)
  ( :map lsp-ui-peek-mode-map
	("j"   . lsp-ui-peek--select-next)
	("k"   . lsp-ui-peek--select-prev)
	("C-k" . lsp-ui-peek--select-prev-file)
	("C-j" . lsp-ui-peek--select-next-file)))

;;;; Latex
;;;; Rust
(use-package rustic
  :mode ("\\.rs%" . rustic-mode)
  :init
  (defalias 'org-babel-execute:rust #'org-babel-execute:rustic)
  (with-eval-after-load "org"
	(require 'rustic-babel))
  :custom
  (rustic-lsp-client 'eglot)
  (lsp-rust-analyzer-server-display-inlay-hints t)
  (rust-indent-method-chain t)
  (rust-babel-format-src-block nil)
  (rust-format-trigger nil))

;;;; SQL
(use-package sql
  :straight (:type built-in)
  :custom
  (sql-ms-program "sqlcmd")
  (sql-ms-options '("-w" "300")))

;;;; C/C++
;;;; Web
;;;; JSON
;;;; yaml
;;;;; JSX
;;;;; TSX
;;;;; HTML
;;;;; CSS
;;;;; Powershell
(use-package powershell
  :if (string-equal system-type 'windows-nt)
  :hook (powershell-mode-hook . lsp)
  :mode ("\\.ps1\\'"))

;;;;; Svelte
;;;;; React
;;;; Shell
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files '("/home/tpb/Documents/org/roam/personal_agenda.org") nil nil "Customized with use-package org-agenda"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
